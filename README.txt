I did all of the work, as the only person on my team.
I first made an AI that chose the first possible move in about 5 minutes. I
failed to commit this, because I just instantly went about implementing the
minmax tree. This failed to work for unknown reasons, until I changed
nothing in the implementation but managed to fix all the problems anyway.
Unclear as to why this occurred. I tested it at this point, and the program
printed out a statement right before it returned successfully, but failed to
actually return.
I then added a heuristic sharply penalizing moves next to corners and 
rewarding coners heavily. My AI thus does very well on the edges of the
board. In the middle, it is about equal to ConstantTimePlayer.
By some stroke of luck, and due to the determinism of BetterPlayer, the AI
beat BetterPlayer, something that I can't do consistently. It does so 
mostly by coming back at the end to take the entire perimeter, something
that happens often with ConstantTimePlayer.
Realistically, I won't do terribly well at the tournament, but I have CS 124
to do instead, so this doesn't particularly trouble me.
