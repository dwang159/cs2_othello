#include "player.h"
#include <vector>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side s) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    board = Board();
    side = s;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

int weight(Move *m)
{
    int x = m->x;
    int y = m->y;
    int ret = 1;

    if (x == 0 || y == 0)
    {
        ret = 2;
    }
    if ((x == 0 && y == 0) || (x == 0 && y == 7) || (x == 7 && y == 0) ||
            (x == 7 && y == 7))
    {
        ret = 600;
    }

    if ((x == 1 && y == 1) || (x == 1 && y == 6) || (x == 6 && y == 6) ||
            (x == 6 && y == 1) || (x == 1 && y == 0) || (x == 0 && y == 1)
            || (x == 6 && y == 0) || (x == 7 && y == 1) ||
            (x == 0 && y == 6) || (x == 1 && y == 7) || (x == 7 && y == 6)
            || (x == 6 && y == 7))
    {
        ret = -10;
    }
    return ret;
}


/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    Side other = (side == BLACK) ? WHITE : BLACK;
    board.doMove(opponentsMove, other);

    std::vector<Move *> possmoves;
    Move *move;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            move = new Move(i,j);
            if (board.checkMove(move, side))
            {
                possmoves.push_back(move);
            }
            else
            {
                delete move;
            }
        }
    }
    std::vector<int> minmax;
    minmax.assign(possmoves.size(), 0);
    Board testboard, oldboard;
    int score, next;
    for (int k = 0; k < possmoves.size(); k++)
    {
        testboard.doMove(possmoves[k], side);
        oldboard = testboard;
        score = testboard.count(side) * weight(possmoves[k]);
        minmax[k] = score;
        Move testmove = Move(0,0);
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                testmove = Move(i, j);
                if (testboard.checkMove((Move *) &testmove, other))
                {
                    testboard.doMove((Move *) &testmove, other);
                    next = testboard.count(side);
                    if (next < minmax[k])
                        minmax[k] = next;
                    testboard = oldboard;
                }
            }
        }
    }
    int best = -1;
    score = -64;
    for (int i = 0; i < minmax.size(); i++)
    {
        if (minmax[i] > score)
        {
            score = minmax[i];
            best = i;
        }
    }

    for (int i = 0; i < possmoves.size(); i++)
    {
        if (i != best)
            delete possmoves[i];
    }
    if (best == -1)
        return NULL;
    board.doMove(possmoves[best], side);
    return possmoves[best];
}

